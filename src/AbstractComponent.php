<?php
declare(strict_types=1);

namespace Trick\Component;

use Nette\Application\UI\Control;


abstract class AbstractComponent extends Control
{
	/** @var string|NULL */
	protected $templateFile;


	public function render(): void
	{
		if (!$this->templateFile) {
			preg_match('#(\w+)$#', get_class($this), $m);
			$this->setTemplate(mb_strtolower($m[1]));
		}

		$this->loadStyle();

		$this->template->setFile($this->templateFile);
		$this->template->componentName = $this->getUniqueId();
		$this->template->render();
	}


	private function loadStyle(): void
	{
		preg_match('#(\w+)$#', get_class($this), $m);
		$name = lcfirst($m[1]);
		$scss = dirname($this->getReflection()->getFileName()) . '/' . $name . '.scss';

		if (file_exists($scss)) {
			$css = str_replace('\\', '/', $scss);
			$appDir = str_replace('\\', '/', APP_DIR);
			$css = str_replace($appDir . '/Module/', '', $css);
			$css = str_replace('/Components', '', $css);
			$css = str_replace($m[1] . '/', '', $css);
			$css = str_replace('.scss', '', $css);
			$css = 'components/' . $css;
			$this->template->getLatte()->invokeFilter('style', [ $css ]);
		}
	}


	public function setTemplate(string $fileName): void
	{
		$dir = dirname($this->getReflection()->getFileName());
		$this->templateFile = $dir . '/' . $fileName . '.latte';
	}


	public function flashMessage($message, string $type = 'info'): \stdClass
	{
		return $this->getPresenter()->flashMessage($message, $type);
	}
}
